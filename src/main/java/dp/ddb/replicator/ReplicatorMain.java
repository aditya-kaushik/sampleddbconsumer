package dp.ddb.replicator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReplicatorMain {
    public static void main(String[] args) {
        SpringApplication.run(ReplicatorMain.class, args);
    }
}
