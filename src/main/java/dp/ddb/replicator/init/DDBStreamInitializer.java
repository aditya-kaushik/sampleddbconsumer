package dp.ddb.replicator.init;

import dp.ddb.replicator.kinesis.StreamsAdapterMain;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class DDBStreamInitializer {

    private StreamsAdapterMain streamsAdapterMain;

    public DDBStreamInitializer(final StreamsAdapterMain streamsAdapterMain) {
        this.streamsAdapterMain = streamsAdapterMain;
    }

    @EventListener
    public void onApplicationEvent(ContextRefreshedEvent event) throws Exception {
        streamsAdapterMain.start();
    }

    @EventListener
    public void onStopEvent(ContextClosedEvent contextClosedEvent){
        streamsAdapterMain.stop();
    }
}